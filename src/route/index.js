import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Splash, GetStarted, Register, Login, UploadPhoto, Messages, Doctor, Hospitals, ChooseDoctor, Chatting, UserProfile, EditProfile, DoctorProfile } from '../pages'
import { BottomNavigator } from '../components';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
    const Off = { headerShown: false }
    return (
        <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
            <Tab.Screen name='Doctor' component={Doctor} options={Off} />
            <Tab.Screen name='Messages' component={Messages} options={Off} />
            <Tab.Screen name='Hospitals' component={Hospitals} options={Off} />
        </Tab.Navigator>
    )
}

const Route = () => {
    const Off = { headerShown: false }
    return (
        <Stack.Navigator initialRouteName='Login'>
            <Stack.Screen name='MainApp' component={MainApp} options={Off} />
            <Stack.Screen name='Splash' component={Splash} options={Off} />
            <Stack.Screen name='GetStarted' component={GetStarted} options={Off} />
            <Stack.Screen name='Register' component={Register} options={Off} />
            <Stack.Screen name='Login' component={Login} options={Off} />
            <Stack.Screen name='UploadPhoto' component={UploadPhoto} options={Off} />
            <Stack.Screen name='ChooseDoctor' component={ChooseDoctor} options={Off} />
            <Stack.Screen name='Chatting' component={Chatting} options={Off} />
            <Stack.Screen name='UserProfile' component={UserProfile} options={Off} />
            <Stack.Screen name='EditProfile' component={EditProfile} options={Off} />
            <Stack.Screen name='DoctorProfile' component={DoctorProfile} options={Off} />
        </Stack.Navigator>
    )
}

export default Route;