import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Route from './route';
import FlashMessage from 'react-native-flash-message';

const App = () => {
  return (
    <>
      <NavigationContainer>
        <Route />
      </NavigationContainer>
      <FlashMessage position="top" />
    </>
  )
}

export default App;