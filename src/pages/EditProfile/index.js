import React, { useEffect, useState } from 'react'
import { StyleSheet, View, ScrollView } from 'react-native'
import { Button, Gap, Header, Input, Profile } from '../../components'
import { colors, getData, storeData } from '../../utils'
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import { showMessage } from 'react-native-flash-message';
import ImagePicker from 'react-native-image-crop-picker';
import { ILNullPhoto } from '../../assets';

const EditProfile = ({ navigation }) => {
    const [profile, setProfile] = useState({
        fullName: '',
        profession: '',
        email: '',
    })

    const [password, setPassword] = useState('')
    const [photo, setPhoto] = useState(ILNullPhoto)
    const [photoForDB, setPhotoForDB] = useState('')

    useEffect(() => {
        getData('user').then(res => {
            const data = res;
            setPhoto({ uri: res.photo })
            setProfile(data)
        })
    }, [])

    const changeText = (key, value) => {
        setProfile({
            ...profile,
            [key]: value
        })
    }

    const update = () => {
        console.log('profile', profile)

        if (password.length > 0) {
            if (password.length < 6) {
                showMessage({
                    message: 'Password kurang dari 6 Karakter!',
                    type: 'danger'
                })
            } else {
                updatePassword();
                updateProfileData();
                navigation.replace('MainApp');
            }
        } else {
            updateProfileData();
            navigation.replace('MainApp');
        }
    }

    const updatePassword = () => {
        // Update Password
        auth()
            .onAuthStateChanged(user => {
                if (user) {
                    user.updatePassword(password).catch(err => {
                        showMessage({
                            message: err.message,
                            type: 'danger'
                        })
                    })
                }
            })
    }

    const updateProfileData = () => {
        const data = profile;
        data.photo = photoForDB;

        database()
            .ref(`/users/${profile.uid}`)
            .update(data)
            .then(() => {
                console.log('Data updated', data);
                storeData('user', data);
            })
            .catch(err => {
                showMessage({
                    message: err.message,
                    type: 'danger'
                })
            });
    }

    const getImage = () => {
        ImagePicker.openPicker({
            compressImageQuality: 0.5,
            compressImageMaxHeight: 200,
            compressImageMaxWidth: 200,
            includeBase64: true
        })
            .then(image => {
                console.log(image);
                const source = { uri: image.path }
                setPhotoForDB(`data:${image.mime};base64,${image.data}`)
                setPhoto(source);
            })
            .catch(() => {
                showMessage({
                    message: "Oops, Sepertinya anda tidak memilih fotonya ?",
                    type: "danger",
                });
            })
    }

    return (
        <View style={{ flex: 1 }}>
            <Header title="Daftar Akun" onPress={() => navigation.goBack()} />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.content}>
                    <Profile isRemove photo={photo} onPress={getImage} />
                    <Input label="Full Name" value={profile.fullName} onChangeText={(value) => changeText('fullName', value)} />
                    <Gap height={24} />
                    <Input label="Pekerjaan" value={profile.profession} onChangeText={(value) => changeText('profession', value)} />
                    <Gap height={24} />
                    <Input disable label="Email Address" value={profile.email} />
                    <Gap height={24} />
                    <Input
                        label="Password"
                        secureTextEntry
                        value={password}
                        onChangeText={value => setPassword(value)}
                    />
                    <Gap height={40} />
                    <Button Title="Save Profile" onPress={update} />
                </View>
            </ScrollView>
        </View>
    )
}

export default EditProfile

const styles = StyleSheet.create({
    content: {
        padding: 40,
        paddingTop: 0,
        backgroundColor: colors.white
    }
})