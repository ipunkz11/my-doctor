import React, { useState } from 'react'
import { Image, Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import { IconAddPhoto, IconRemovePhoto, ILNullPhoto } from '../../assets'
import { Button, Gap, Link } from '../../components/atoms'
import Header from '../../components/molecules/Header'
import { colors, Fonts, getData, storeData } from '../../utils'
import ImagePicker from 'react-native-image-crop-picker';
import { showMessage } from "react-native-flash-message";
import database from '@react-native-firebase/database';

const UploadPhoto = ({ navigation, route }) => {
    const { fullName, profession, uid } = route.params
    const [hasPhoto, setHasPhoto] = useState(false)
    const [photo, setPhoto] = useState(ILNullPhoto)
    const [photoForDB, setPhotoForDB] = useState('')

    const uploadGallery = () => {
        ImagePicker.openPicker({
            compressImageQuality: 0.5,
            compressImageMaxHeight: 200,
            compressImageMaxWidth: 200,
            includeBase64: true
        })
            .then(image => {
                console.log(image);
                const source = { uri: image.path }
                setPhotoForDB(`data:${image.mime};base64,${image.data}`)
                setPhoto(source);
                setHasPhoto(true)
            })
            .catch(() => {
                showMessage({
                    message: "Oops, Sepertinya anda tidak memilih fotonya ?",
                    type: "danger",
                });
            })
    }

    const uploadAndContiue = () => {
        database()
        .ref('users/' + uid + '/')
        .update({photo: photoForDB})

        const data = route.params
        const photo = photoForDB
        const datas = {...data, photo}

        storeData('user', datas)

        navigation.replace('MainApp')
    }
    return (
        <View style={styles.page}>
            <Header title="Upload Photo" onPress={() => navigation.goBack()} />
            <View style={styles.content}>
                <View style={styles.profile}>
                    <TouchableOpacity style={styles.wrapAvatar} onPress={uploadGallery}>
                        <Image source={photo} style={styles.avatar} />
                        {hasPhoto && <IconRemovePhoto style={styles.addPhoto} />}
                        {!hasPhoto && <IconAddPhoto style={
                            styles.addPhoto} />}
                    </TouchableOpacity>
                    <Text style={styles.name}>{fullName}</Text>
                    <Text style={styles.profession}>{profession}</Text>
                </View>

                <View>
                    <Button disable={!hasPhoto} Title="Upload and Continue" onPress={uploadAndContiue} />
                    <Gap height={30} />
                    <Link label="Skip for this" align="center" size={16} onPress={() => navigation.replace('MainApp')} />
                </View>
            </View>
        </View>
    )
}

export default UploadPhoto

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: 'white',
    },
    content: {
        paddingHorizontal: 40,
        paddingBottom: 60,
        flex: 1,
        justifyContent: 'space-between'
    },
    profile: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    wrapAvatar: {
        width: 130,
        height: 130,
        borderWidth: 1,
        borderColor: colors.border,
        borderRadius: 130 / 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatar: {
        width: 110,
        height: 110,
        borderRadius: 110 / 2
    },
    addPhoto: {
        position: 'absolute',
        bottom: 8,
        right: 6,
    },
    name: {
        fontSize: 24,
        color: colors.text.primary,
        fontFamily: Fonts.primary[600],
        textAlign: 'center'
    },
    profession: {
        fontSize: 18,
        fontFamily: Fonts.primary.normal,
        textAlign: 'center',
        color: colors.text.secondary,
    }
})