import React, { useEffect } from "react";
import { View, Text, StyleSheet, ScrollView } from 'react-native'
import { DummyDokter1, DummyDokter2, DummyDokter3, JSONCategoryDoctor } from "../../assets";
import { DoctorCategory, Gap, HomeProfile, NewsItem, RatedDoctor } from "../../components";
import { colors, Fonts } from "../../utils";

const Doctor = ({navigation}) => {
    return (
        <View style={styles.page}>
            <View style={styles.content}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Gap height={30} />
                    <View style={styles.profile}>
                        <HomeProfile onPress={() => navigation.navigate('UserProfile')} />
                        <Text style={styles.welcome}>Mau konsultasi dengan
                            siapa hari ini?</Text>
                    </View>
                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    <View style={styles.category}>
                        {JSONCategoryDoctor.data.map(item => {
                            return(
                                <DoctorCategory key={item.id} category={item.category} onPress={ () => navigation.navigate('ChooseDoctor')} />
                            )
                        })}
                    </View>
                    </ScrollView>
                    <Text style={styles.label}>Top Rated Doctors</Text>
                    <View style={styles.rated}>
                        <RatedDoctor name="Alexa Rachel" desc="Pediatrician" avatar={DummyDokter1} onPress={() => navigation.navigate('DoctorProfile')} />
                        <RatedDoctor name="Sunny Frank" desc="Dentist" avatar={DummyDokter2} />
                        <RatedDoctor name="Poe Minn" desc="Podiatrist" avatar={DummyDokter3} />
                    </View>
                    <Text style={styles.label}>Good News</Text>
                    <View style={styles.news}>
                    <NewsItem />
                    <NewsItem />
                    <NewsItem />
                    </View>
                    <Gap height={30} />
                </ScrollView>
            </View>
        </View>
    )
}

export default Doctor

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.secondary,
        flex: 1
    },
    content: {
        backgroundColor: colors.white,
        flex: 1,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    profile: {
        paddingHorizontal: 16
    },
    welcome: {
        fontSize: 20,
        fontFamily: Fonts.primary[600],
        color: colors.text.primary,
        marginTop: 30,
        marginBottom: 16,
        maxWidth: 250
    },
    category: {
        paddingHorizontal: 16,
        flexDirection: 'row',
    },
    rated: {
        paddingHorizontal: 16
    },
    label: {
        fontSize: 16,
        fontFamily: Fonts.primary[600],
        color: colors.text.primary,
        marginTop: 30,
        marginBottom: 16,
        paddingHorizontal: 16
    },
    news:{
        paddingHorizontal: 16
    }
})
