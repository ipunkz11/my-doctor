import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { ChatItem, Header, InputChat } from '../../components'
import { colors, Fonts } from '../../utils'

const Chatting = ({ navigation }) => {
    return (
        <View style={styles.page}>
            <Header title="Nairobi Putri Hayza" type="dark-profile" onPress={() => navigation.goBack()} />
            <View style={styles.container}>
                <Text style={styles.date}>Senin, 21 Maret, 2020</Text>
                <ChatItem isMe />
                <ChatItem />
                <ChatItem isMe />
            </View>
            <InputChat />
        </View>
    )
}

export default Chatting

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },
    container: {
        flex: 1,
    },
    date: {
        fontSize: 11,
        fontFamily: Fonts.primary[400],
        color: colors.text.secondary,
        marginTop: 20,
        textAlign: 'center'
    }
})
