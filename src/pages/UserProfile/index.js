import React, { useEffect, useState } from 'react'
import { View, StyleSheet } from 'react-native'
import { ILNullPhoto } from '../../assets'
import { Header, ListDoctor, Profile } from '../../components'
import { colors, getData } from '../../utils'
import auth from '@react-native-firebase/auth';
import { showMessage } from 'react-native-flash-message'

const UserProfile = ({ navigation }) => {
    const [profile, setProfile] = useState({
        fullName: '',
        profession: '',
        photo: ILNullPhoto
    })

    useEffect(() => {
        getData('user').then(res => {
            // console.log(res)
            const data = res
            data.photo = { uri: res.photo }
            setProfile(data)
        })
    }, [])

    const signOut = () => {
        auth()
            .signOut().then(() => {
                console.log('SignOut Success');
                navigation.replace('GetStarted');
            })
            .catch(err => {
                showMessage({
                    message: err.message,
                    type: 'danger'
                })
            })
    }

    return (
        <View style={styles.page}>
            <Header title="Profile" onPress={() => navigation.goBack()} />
            {profile.fullName.length > 0 && <Profile name={profile.fullName} desc={profile.profession} photo={profile.photo} />}
            <ListDoctor name="Edit Profile" desc="Last updated yesterday" type="next" icon="edit profile" onPress={() => navigation.navigate('EditProfile')} />
            <ListDoctor name="Languange" desc="Available 12 languanges" type="next" icon="language" />
            <ListDoctor name="Give Us Rate" desc="On Google Play Store" type="next" icon="rate" />
            <ListDoctor name="Sign Out" desc="Read our guidelines" type="next" icon="help" onPress={signOut} />
        </View>
    )
}

export default UserProfile

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.white,
        flex: 1
    }
})
