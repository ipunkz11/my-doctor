import React from 'react'
import { View, Text } from 'react-native'
import { DummyDokter1, DummyDokter2, DummyDokter3 } from '../../assets'
import {Header, ListDoctor} from '../../components'

const ChooseDoctor = ({navigation}) => {
    return (
        <View>
            <Header title="Pilih Dokter Anak" type="dark" onPress={() => navigation.goBack()} />
            <ListDoctor type="next" name="Alexander Jannie" desc="Wanita" pic={DummyDokter1} onPress={() => navigation.navigate('Chatting')} />
            <ListDoctor type="next" name="John McParker Steve" desc="Laki - laki" pic={DummyDokter2} onPress={() => navigation.navigate('Chatting')} />
            <ListDoctor type="next" name="Nayrobi Putri Hayza" desc="Wanita" pic={DummyDokter3} onPress={() => navigation.navigate('Chatting')} />
            <ListDoctor type="next" name="James Rivillia" desc="Laki - laki" pic={DummyDokter2} onPress={() => navigation.navigate('Chatting')} />
            <ListDoctor type="next" name="Liu Yue Tian Park" desc="Wanita" pic={DummyDokter3} onPress={() => navigation.navigate('Chatting')} />
        </View>
    )
}

export default ChooseDoctor