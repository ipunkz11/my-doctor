import React, { useState } from 'react'
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import { StyleSheet, View } from 'react-native'
import { Button, Gap, Header, Input, Loading } from '../../components'
import { colors, storeData, useForm } from '../../utils'
import { showMessage } from "react-native-flash-message";

const Register = ({ navigation }) => {
    const [form, setForm] = useForm({
        fullName: '',
        profession: '',
        email: '',
        password: '',
    })

    const [loading, setLoading] = useState(false)

    const onContinue = () => {
        console.log(form)
        setLoading(true)
        auth()
            .createUserWithEmailAndPassword(form.email, form.password)
            .then((success) => {
                setLoading(false)
                setForm('reset')
                console.log('User account created & signed in!', success);

                const data = {
                    fullName: form.fullName,
                    profession: form.profession,
                    email: form.email,
                    uid: success.user.uid
                }

                database()
                    .ref('users/' + success.user.uid + '/')
                    .set(data)

                storeData('user', data)

                navigation.navigate('UploadPhoto', data)
            })
            .catch(error => {
                const errorMessage = error.message
                setLoading(false)
                showMessage({
                    message: errorMessage,
                    type: "danger",
                })
            });
    }


    return (
        <>
            <View style={styles.page}>
                <Header title="Daftar Akun" onPress={() => navigation.goBack()} />
                <View style={styles.content}>
                    <Input label="Full Name" value={form.fullName} onChangeText={(value) => setForm('fullName', value)} />
                    <Gap height={24} />
                    <Input label="Pekerjaan" value={form.profession} onChangeText={(value) => setForm('profession', value)} />
                    <Gap height={24} />
                    <Input label="Email Address" value={form.email} onChangeText={(value) => setForm('email', value)} />
                    <Gap height={24} />
                    <Input label="Password" value={form.password} onChangeText={(value) => setForm('password', value)} secureTextEntry />
                    <Gap height={40} />
                    <Button Title="Continue" onPress={onContinue} />
                </View>
            </View>
            {loading && <Loading />}
        </>
    )
}

export default Register

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: colors.white
    },
    content: {
        padding: 40,
        paddingTop: 0,
    }
})
