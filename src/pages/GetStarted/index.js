import React from 'react'
import { View, Text, StyleSheet, ImageBackground } from 'react-native'
import { ILLogo, ILGetStarted } from '../../assets'
import { Button, Gap } from '../../components'
import { colors } from '../../utils'

const GetStarted = ({navigation}) => {
  return (
    <ImageBackground source={ILGetStarted} style={styles.page}>
      <View>
        <ILLogo />
        <Text style={styles.title}>Pesan Kopimu sekarang lalu nikmati sambil rebahan</Text>
      </View>
      <View>
        <Button Title="Get Started" onPress={() => navigation.navigate('Register')} />
        <Gap height={16} />
        <Button type="secondary" Title="Sign In" onPress={() => navigation.navigate('Login')} />
      </View>
    </ImageBackground>
  )
}

export default GetStarted

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
    justifyContent: 'space-between',
    padding: 40,
  },
  title: {
    fontSize: 28,
    fontWeight: '600',
    color: colors.white,
    marginTop: 90,
  }
})