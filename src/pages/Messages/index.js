import React from "react";
import { View, Text, StyleSheet } from 'react-native'
import { DummyDokter1, DummyDokter2, DummyDokter3 } from "../../assets";
import { ListDoctor } from "../../components";
import { colors, Fonts } from "../../utils";

const Messages = ({navigation}) => {
    return (
        <View style={styles.page}>
            <View style={styles.content}>
                <Text style={styles.title}>Messages</Text>
                <ListDoctor pic={DummyDokter1} name="Alexander Jannie" desc="Baik ibu, terima kasih banyak atas wakt..." onPress={() => navigation.navigate('Chatting')} />
                <ListDoctor pic={DummyDokter2} name="Nairobi Putri Hayza" desc="Oh tentu saja tidak karena jeruk it..." />
                <ListDoctor pic={DummyDokter3} name="John McParker Steve" desc="Oke menurut pak dokter bagaimana unt..." />
            </View>
        </View>
    )
}

export default Messages

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.secondary,
        flex: 1
    },
    content: {
        backgroundColor: colors.white,
        flex: 1,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20
    },
    title: {
        fontSize: 20,
        fontFamily: Fonts.primary[600],
        color: colors.text.primary,
        marginTop: 30,
        marginLeft: 16
    }
})
