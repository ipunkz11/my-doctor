import React from 'react'
import { StyleSheet, View } from 'react-native'
import { Button, Gap, Header, Profile, ProfileItem } from '../../components'
import { colors } from '../../utils'

const DoctorProfile = ({ navigation }) => {
    return (
        <View style={styles.page}>
            <View>
                <Header title="Profile" onPress={() => navigation.goBack()} />
                <Profile name="Nayrobi Putri Hayza" desc="Dokter Anak" />
                <Gap height={10} />
                <ProfileItem name="Alumnus" value="Universitas Indonesia, 2020" />
                <ProfileItem name="Tempat Praktik" value="Rumah Sakit Umum, Bandung" />
                <ProfileItem name="No STR" value="0000116622081996" />
            </View>
            <View style={styles.action}>
                <Button Title="Start Consultation" onPress={() => navigation.navigate('Chatting')} />
            </View>
        </View>
    )
}

export default DoctorProfile

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: colors.white,
        justifyContent: 'space-between'
    },
    action: {
        padding: 40
    }
})