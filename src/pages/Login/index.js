import React, { useState } from 'react'
import { Text, View, StyleSheet, ScrollView } from 'react-native'
import { ILLogo } from '../../assets'
import { Button, Gap, Input, Link, Loading } from '../../components'
import { colors, Fonts, storeData, useForm } from '../../utils'
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import { showMessage } from 'react-native-flash-message'

const Login = ({ navigation }) => {
    const [loading, setLoading] = useState(false)
    const [form, setForm] = useForm({
        email: '',
        password: ''
    })

    const login = () => {
        console.log(form)
        setLoading(true)
        auth()
            .signInWithEmailAndPassword(form.email, form.password)
            .then(res => {
                setLoading(false)
                setForm('reset')
                console.log('Success', res)
                database()
                    .ref(`users/${res.user.uid}/`)
                    .once('value')
                    .then(resDB => {
                        console.log('data user :', resDB.val())
                        if (resDB.val()) {
                            storeData('user', resDB.val())
                            navigation.replace('MainApp')
                        }
                    })
            })
            .catch(error => {
                setLoading(false)
                showMessage({
                    message: error.message,
                    type: "danger",
                })
            });
    }

    return (
        <>
            <View style={styles.page}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Gap height={40} />
                    <ILLogo />
                    <Text style={styles.title}>Masuk dan mulai berkonsultasi</Text>
                    <Input label="Email Addres" value={form.email} onChangeText={(value) => setForm('email', value)} />
                    <Gap height={24} />
                    <Input secureTextEntry label="Password" value={form.password} onChangeText={(value) => setForm('password', value)} />
                    <Gap height={10} />
                    <Link label="Forgot My Password" size={12} />
                    <Gap height={40} />
                    <Button Title="Sign In" onPress={login} />
                    <Gap height={30} />
                    <Link label="Create New Account" size={16} align="center" onPress={() => navigation.navigate('Register')} />
                </ScrollView>
            </View>
            {loading && <Loading />}
        </>
    )
}

export default Login

const styles = StyleSheet.create({
    page: {
        flex: 1,
        paddingHorizontal: 40,
    },
    title: {
        fontSize: 20,
        fontFamily: Fonts.primary[600],
        color: colors.text.primary,
        maxWidth: 200,
        marginVertical: 40,
    }
})