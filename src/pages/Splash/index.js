import React, {useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native'
import { ILLogo } from '../../assets';
import { colors, Fonts } from '../../utils';
import auth from '@react-native-firebase/auth';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      auth().onAuthStateChanged(user => {
        if(user){
          console.log(user)
          navigation.replace('MainApp')
        }else{
          navigation.replace('GetStarted')
        }
      })
    }, 3000);
  }, [navigation]);
  return(
    <View style={styles.head}>
      <ILLogo/>
      <Text style={styles.title}>Baristas</Text>
    </View>
  )
}

export default Splash;

const styles = StyleSheet.create({
  head:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  },
  title:{
    fontSize:20,
    fontFamily:Fonts.primary[600],
    color:colors.text.primary,
    marginTop:20,
  }
})