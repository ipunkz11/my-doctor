import React, {useState} from "react";
import { View, Text, ImageBackground, StyleSheet } from 'react-native'
import { DummyHospital1, DummyHospital2, DummyHospital3, ILHospitalBG } from "../../assets";
import { ListRS } from "../../components";
import { colors, Fonts } from "../../utils"

const Hospitals = () => {
    const [doctors] = useState([
        {
            id: 1,
            pic: DummyHospital1,
            title: 'Rumah Sakit',
            name: 'Citra Bunga Merdeka',
            address: ' Jln. Surya Sejahtera 20 '
        },
        {
            id: 2,
            pic: DummyHospital2,
            title: 'Rumah Sakit Anak',
            name: 'Citra Bunga Merdeka',
            address: 'Happy Family & Kids'
        },
        {
            id: 3,
            pic: DummyHospital3,
            title: 'Rumah Sakit Jiwa',
            name: 'Citra Bunga Merdeka',
            address: 'Tingkatan Paling Atas'
        }
    ])
    return (
        <View style={styles.page}>
            <ImageBackground source={ILHospitalBG} style={styles.background}>
                <Text style={styles.name}>Nearby Hospitals</Text>
                <Text style={styles.desc}>3 tersedia</Text>
            </ImageBackground>
            <View style={styles.container}>
                {doctors.map(doctor => {
                    return(
                        <ListRS key={doctor.id} pic={doctor.pic} title={doctor.title} name={doctor.name} address={doctor.address} />
                    )
                })}
            </View>
        </View>
    )
}

export default Hospitals

const styles = StyleSheet.create({
    container: {
        marginTop: -20,
        backgroundColor: colors.white,
        borderRadius: 20,
        flex: 1,
        paddingTop: 20
    },
    page: {
        backgroundColor: colors.secondary,
        flex: 1
    },
    background: {
        height: 240,
        paddingTop: 30,
        alignItems: 'center',
    },
    name: {
        fontSize: 20,
        fontFamily: Fonts.primary[600],
        color: colors.white
    },
    desc: {
        fontSize: 14,
        fontFamily: Fonts.primary[300],
        color: colors.white
    }
})
