const mainColors = {
    green1: "#0BCAD4",
    green2: "#EDFCFD",
    dark1: "#112340",
    dark2: "#495A75",
    dark3: "#0066CB",
    grey1: "#7D8797",
    grey2: "#E9E9E9",
    grey3: "#8092AF",
    grey4: '#EDEEF0',
    grey5: '#B1B7C2',
    black1: '#000000',
    black2: 'rgba(0, 0, 0, 0.5)'
}

export const colors = {
    primary: mainColors.green1,
    secondary: mainColors.dark1,
    tertiery: mainColors.dark3,
    white: 'white',
    black: 'black',
    text: {
        primary: mainColors.dark1,
        secondary: mainColors.grey1,
        menuInactive: mainColors.dark2,
        menuActive: mainColors.green1,
        subTitle: mainColors.grey3
    },
    button: {
        primary: {
            background: mainColors.green1,
            text: 'white',
        },
        secondary: {
            background: 'white',
            text: mainColors.dark1,
        },
        disable: {
            background: mainColors.grey3,
            text: mainColors.grey5,
        },
    },
    textInput: mainColors.grey4,
    border: mainColors.grey2,
    cardLight: mainColors.green2,
    loadingBackground: mainColors.black2
};