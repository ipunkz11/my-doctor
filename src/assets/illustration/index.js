import ILLogo from './Logo.svg'
import ILGetStarted from './GetStarted.png'
import ILNullPhoto from './null_photo.png'
import ILCatUmum from './cat_dokter_umum.svg'
import ILCatPsikiater from './cat_dokter_psikiater.svg'
import ILCatObat from './cat_dokter_obat.svg'
import ILHospitalBG from './BackGround.png'

export { ILLogo, ILGetStarted, ILNullPhoto, ILCatUmum, ILCatPsikiater, ILCatObat, ILHospitalBG }