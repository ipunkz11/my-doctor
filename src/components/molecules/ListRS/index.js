import React from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import { DummyHospital1 } from '../../../assets'
import { colors, Fonts } from '../../../utils'

const ListRS = ({pic, title, name, address}) => {
    return (
        <View style={styles.container}>
            <Image source={pic} style={styles.image} />
            <View>
                <Text style={styles.name}>{title}</Text>
                <Text style={styles.name}>{name}</Text>
                <Text style={styles.address}>{address}</Text>
            </View>
        </View>
    )
}

export default ListRS

const styles = StyleSheet.create({
    
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: colors.border,
        padding: 16,
    },
    image: {
        borderRadius: 20,
        marginRight: 12,
        width: 110,
        height: 80,
    },
    name: {
        fontSize: 16,
        fontFamily: Fonts.primary[400],
        color: colors.text.primary,
    },
    address: {
        fontSize: 12,
        fontFamily: Fonts.primary[300],
        color: colors.text.secondary,
        marginTop: 6
    }
})
