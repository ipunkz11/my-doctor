import React from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { DummyUser, IconRemovePhoto } from '../../../assets'
import { colors, Fonts } from '../../../utils'

const Profile = ({ name, desc, isRemove, photo, onPress }) => {
    return (
        <View style={styles.page}>
            {!isRemove && (
                <View style={styles.container}>
                    <Image style={styles.avatar} source={photo} />
                </View>
            )}
            {isRemove && (
                <TouchableOpacity style={styles.container} onPress={onPress}>
                    <Image style={styles.avatar} source={photo} />
                    {isRemove && <IconRemovePhoto style={styles.removePhoto} />}
                </TouchableOpacity>
            )}
            {name && (
                <View style={styles.content}>
                    <Text style={styles.name}>{name}</Text>
                    <Text style={styles.Profession}>{desc}</Text>
                </View>
            )}
        </View>
    )
}

export default Profile

const styles = StyleSheet.create({
    page: {
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 30,
    },
    container: {
        height: 130,
        width: 130,
        borderRadius: 130 / 2,
        borderWidth: 1,
        marginBottom: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    content: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatar: {
        height: 110,
        width: 110,
        borderRadius: 110 / 2
    },
    name: {
        fontSize: 20,
        fontFamily: Fonts.primary[600],
        color: colors.text.primary
    },
    Profession: {
        fontSize: 16,
        fontFamily: Fonts.primary[400],
        color: colors.text.secondary
    },
    removePhoto: {
        position: 'absolute',
        right: 6,
        bottom: 4
    }
})
