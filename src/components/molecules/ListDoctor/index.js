import React from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { IconEditProfile, IconHelp, IconLanguage, IconNext, IconRate } from '../../../assets'
import { colors, Fonts } from '../../../utils'

const ListDoctor = ({ name, desc, pic, type, onPress, icon }) => {
    const Icon = () => {
        if(icon === 'edit profile'){
            return <IconEditProfile />
        }
        if(icon === 'language'){
            return <IconLanguage />
        }
        if(icon === 'rate'){
            return <IconRate />
        }
        if(icon === 'help'){
            return <IconHelp />
        }
        return <IconEditProfile />
    }
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            {icon ? <Icon /> : <Image source={pic} style={styles.avatar} />}
            <View style={styles.content}>
                <Text style={styles.name}>{name}</Text>
                <Text style={styles.desc}>{desc}</Text>
            </View>
            {type === "next" && <IconNext />}
        </TouchableOpacity>
    )
}

export default ListDoctor

const styles = StyleSheet.create({
    avatar: {
        width: 46,
        height: 46,
        borderRadius: 46 / 2,
        marginRight: 12
    },
    container: {
        flexDirection: 'row',
        marginTop: 16,
        borderBottomWidth: 1,
        borderBottomColor: colors.border,
        alignItems: 'center',
        paddingHorizontal: 16,
        paddingBottom: 20,
        justifyContent: 'space-between'
    },
    content: { flex: 1, marginLeft: 16 },
    name: {
        fontSize: 16,
        fontFamily: Fonts.primary[400],
        color: colors.text.primary
    },
    desc: {
        fontSize: 12,
        fontFamily: Fonts.primary[300],
        color: colors.text.secondary
    }
})
