import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import { DummyNews1 } from '../../../assets'
import { colors, Fonts } from '../../../utils'

const NewsItem = () => {
    return (
        <View style={styles.container}>
            <View style={styles.wrap}>
                <Text style={styles.title}>Is it safe to stay at home
                    during coronavirus?</Text>
                <Text style={styles.date}>Today</Text>
            </View>
            <Image source={DummyNews1} style={styles.image} />
        </View>
    )
}

export default NewsItem;

const styles = StyleSheet.create({
    image: {
        width: 80,
        height: 60,
        borderRadius: 11
    },
    title: {
        fontSize: 16,
        fontFamily: Fonts.primary[600],
        color: colors.text.primary
    },
    date: {
        fontSize: 12,
        fontFamily: Fonts.primary.normal,
        color: colors.text.secondary,
        marginTop: 4
    },
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: colors.border,
        paddingBottom: 12,
        paddingTop: 16
    },
    wrap: {
        flex: 1
    }
})
