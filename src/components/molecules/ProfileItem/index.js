import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { colors, Fonts } from '../../../utils'

const ProfileItem = ({ name, value }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.name}>{name}</Text>
            <Text style={styles.value}>{value}</Text>
        </View>
    )
}

export default ProfileItem

const styles = StyleSheet.create({
    container: {
        padding: 16,
        borderBottomWidth: 1,
        borderBottomColor: colors.border
    },
    name: {
        fontSize: 14,
        fontFamily: Fonts.primary[400],
        color: colors.text.secondary
    },
    value: {
        fontSize: 14,
        fontFamily: Fonts.primary[400],
        color: colors.text.primary
    },
})