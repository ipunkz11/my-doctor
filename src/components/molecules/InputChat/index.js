import React from 'react'
import { View, TextInput, StyleSheet } from 'react-native'
import { colors } from '../../../utils'
import { Button } from '../../atoms'

const InputChat = () => {
    return (
        <View style={styles.page}>
            <TextInput placeholder='Tulis pesan untuk Nayrobi' style={styles.input} />
            <Button type="icon-send" disable />
        </View>
    )
}

export default InputChat

const styles = StyleSheet.create({
    page: {
        flexDirection: 'row',
        marginHorizontal: 16,
        marginVertical: 16,
    },
    input: {
        padding: 14,
        backgroundColor: colors.textInput,
        color: colors.text.primary,
        borderRadius: 10,
        flex: 1,
        marginRight: 10,
        maxHeight: 45
    }
})
