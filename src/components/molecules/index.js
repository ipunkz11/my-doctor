import Header from "./Header";
import BottomNavigator from "./BottomNavigator";
import HomeProfile from "./HomeProfile";
import DoctorCategory from "./DoctorCategory";
import RatedDoctor from "./RatedDoctor";
import NewsItem from "./NewsItem"
import ListDoctor from "./ListDoctor";
import ListRS from "./ListRS";
import ChatItem from "./ChatItem";
import InputChat from "./InputChat";
import DarkProfile from "./Header/dark-profile";
import Other from "./ChatItem/Other";
import ChatMe from "./ChatItem/ChatMe";
import Profile from "./Profile";
import ProfileItem from "./ProfileItem";
import Loading from "./Loading";

export { Header, BottomNavigator, HomeProfile, DoctorCategory, RatedDoctor, NewsItem, ListDoctor, ListRS, ChatItem, InputChat, DarkProfile, Other, ChatMe, Profile, ProfileItem, Loading }