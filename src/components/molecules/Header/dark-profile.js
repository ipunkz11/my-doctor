import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { DummyDokter1 } from '../../../assets'
import { colors, Fonts } from '../../../utils'
import { Button } from '../../atoms'

const DarkProfile = ({onPress}) => {
    return (
        <View style={styles.container}>
            <Button type="icon-only" icon="back-light" onPress={onPress} />
            <View>
                <Text style={styles.name}>Nayrobi Putri Hayza</Text>
                <Text style={styles.job}>Dokter Anak</Text>
            </View>
            <Image source={DummyDokter1} style={styles.avatar} />
        </View>
    )
}

export default DarkProfile

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.secondary,
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 30,
        paddingHorizontal: 16,
        justifyContent: 'space-between',
        borderBottomRightRadius: 20,
        borderBottomStartRadius: 20
    },
    name: {
        fontSize: 20,
        fontFamily: Fonts.primary[600],
        color: colors.white,
    },
    job: {
        fontSize: 11,
        fontFamily: Fonts.primary[400],
        marginTop: 6,
        color: colors.text.subTitle,
        textAlign: 'center'
    },
    avatar: {
        height: 50,
        width: 50,
        borderRadius: 50 / 2
    },
})
