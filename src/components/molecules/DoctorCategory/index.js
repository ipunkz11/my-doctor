import React from 'react'
import {Text, StyleSheet, TouchableOpacity} from 'react-native'
import { ILCatObat, ILCatPsikiater, ILCatUmum } from '../../../assets';
import { colors, Fonts } from '../../../utils';

const DoctorCategory = ({category, onPress}) => {
    const Icon = () => {
        if (category === "Dokter Umum"){
            return <ILCatUmum style={styles.ilustration} />
        }
        if (category === "Psikiater"){
            return <ILCatPsikiater style={styles.ilustration} />
        }
        if (category === "Dokter Obat"){
            return <ILCatObat style={styles.ilustration} />
        }
        return <ILCatUmum style={styles.ilustration} />
    }
    return(
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Icon />
            <Text style={styles.label}>Saya butuh</Text>
            <Text style={styles.category}>{category}</Text>
        </TouchableOpacity>
    )
}

export default DoctorCategory;

const styles = StyleSheet.create({
    container:{
        backgroundColor: colors.cardLight,
        padding: 12,
        borderRadius: 10,
        marginRight: 10,
        height: 130,
        alignSelf: 'flex-start'
    },
    ilustration:{
        marginBottom: 28
    },
    label:{
        fontSize: 12,
        fontFamily: Fonts.primary[300],
        color: colors.text.primary
    },
    category:{
        fontSize: 12,
        fontFamily: Fonts.primary[600],
        color: colors.text.primary
    }
})
