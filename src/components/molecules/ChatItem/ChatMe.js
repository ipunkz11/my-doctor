import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { colors, Fonts } from '../../../utils'

const ChatMe = () => {
    return (
        <View style={styles.page}>
            <View style={styles.container}>
                <Text style={styles.desc}>
                    Ibu dokter, apakah memakan
                    jeruk tiap hari itu buruk?
                </Text>
            </View>
            <Text style={styles.date}>4.20 AM</Text>
        </View>
    )
}

export default ChatMe

const styles = StyleSheet.create({
    page: {
        alignItems: 'flex-end',
        marginVertical: 20,
        marginRight: 16
    },
    container: {
        borderRadius: 10,
        borderBottomRightRadius: 0,
        padding: 12,
        paddingRight: 18,
        maxWidth: '60%',
        backgroundColor: colors.cardLight,
        marginBottom: 8
    },
    desc: {
        fontSize: 14,
        fontFamily: Fonts.primary[400],
        color: colors.text.primary,
    },
    date: {
        fontSize: 11,
        fontFamily: Fonts.primary[400],
        color: colors.text.secondary
    },
})
