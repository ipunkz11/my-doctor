import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { DummyDokter1 } from '../../../assets'
import { colors, Fonts } from '../../../utils'

const Other = () => {
    return (
        <View style={styles.page}>
            <Image source={DummyDokter1} style={styles.avatar} />
            <View>
                <View style={styles.container}>
                    <Text style={styles.desc}>Oh tentu saja tidak karena
                        jeruk itu sangat sehat...</Text>
                </View>
                <Text style={styles.date}>4.45 AM</Text>
            </View>
        </View>
    )
}

export default Other

const styles = StyleSheet.create({
    page: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    container: {
        backgroundColor: colors.primary,
        borderRadius: 10,
        borderBottomLeftRadius: 0,
        maxWidth: '80%',
        padding: 12,
        marginBottom: 8
    },
    desc: {
        fontSize: 14,
        fontFamily: Fonts.primary[400],
        color: colors.white
    },
    date: {
        fontSize: 11,
        fontFamily: Fonts.primary[400],
        color: colors.text.secondary
    },
    avatar: {
        height: 46,
        width: 46,
        borderRadius: 46 / 2,
        marginRight: 12
    }
})
