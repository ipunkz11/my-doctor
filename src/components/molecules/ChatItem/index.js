import React from 'react'
import ChatMe from './ChatMe'
import Other from './Other'

const ChatItem = ({isMe}) => {
    if(isMe){
        return <ChatMe />
    }
    return <Other />
}

export default ChatItem