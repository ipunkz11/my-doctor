import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { IconStar } from '../../../assets'
import { colors, Fonts } from '../../../utils'

const RatedDoctor = ({ onPress, avatar, name, desc }) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Image source={avatar} style={styles.avatar} />
            <View style={styles.profile}>
                <Text style={styles.name}>{name}</Text>
                <Text style={styles.profession}>{desc}</Text>
            </View>
            <View style={styles.rate}>
                <IconStar />
                <IconStar />
                <IconStar />
                <IconStar />
                <IconStar />
            </View>
        </TouchableOpacity>
    )
}

export default RatedDoctor;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 16
    },
    avatar: {
        width: 50,
        height: 50,
        borderRadius: 50 / 2,
        marginRight: 12
    },
    rate: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    name: {
        fontSize: 16,
        fontFamily: Fonts.primary[600],
        color: colors.text.primary
    },
    profession: {
        fontSize: 12,
        fontFamily: Fonts.primary.normal,
        color: colors.text.secondary,
        marginTop: 2
    },
    profile: {
        flex: 1,
    }
})
