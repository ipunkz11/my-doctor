import React from 'react'
import { Text, TouchableOpacity, StyleSheet } from 'react-native'
import { IconDoctor, IconDoctorAktive, IconHospitals, IconHospitalsAktive, IconMessages, IconMessagesAktive } from '../../../assets'
import { colors, Fonts } from '../../../utils'

const TabItem = ({ title, active, onPress, onLongPress }) => {
    const Icon = () => {
        if (title === 'Doctor') {
            return active ? <IconDoctorAktive /> : <IconDoctor />
        }
        if (title === 'Messages') {
            return active ? <IconMessagesAktive /> : <IconMessages />
        }
        if (title === 'Hospitals') {
            return active ? <IconHospitalsAktive /> : <IconHospitals />
        }
        return <IconDoctor />
    }
    return (
        <TouchableOpacity style={styles.container} onPress={onPress} onLongPress={onLongPress}>
            <Icon />
            <Text style={styles.text(active)}>{title}</Text>
        </TouchableOpacity>
    )
}

export default TabItem

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    text: active => ({
        fontSize: 10,
        color: active ? colors.text.menuActive : colors.text.menuInactive,
        fontFamily: Fonts.primary[600],
        marginTop: 4,
    })
})