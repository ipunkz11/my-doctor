import React, { useState } from 'react'
import { View, Text, TextInput, StyleSheet } from 'react-native'
import { colors, Fonts } from '../../../utils';

const Input = ({ label, value, onChangeText, secureTextEntry, disable }) => {
    const [border, setBorder] = useState(colors.border)

    const onFocusForm = () => {
        setBorder(colors.tertiery)
    }
    const onBlurFrom = () => {
        setBorder(colors.border)
    }
    return (
        <View>
            <Text style={styles.label}>{label}</Text>
            <TextInput
                value={value}
                onChangeText={onChangeText}
                onFocus={onFocusForm}
                onBlur={onBlurFrom}
                secureTextEntry={secureTextEntry}
                editable={!disable}
                selectTextOnFocus={!disable}
                style={styles.input(border)}
            />
        </View>
    )
}

export default Input;

const styles = StyleSheet.create({
    label: {
        fontSize: 16,
        fontFamily: Fonts.primary[400],
        color: colors.text.secondary,
        marginBottom: 6,
    },
    input: (border) => (
        {
            borderWidth: 1,
            borderRadius: 10,
            borderColor: border,
            padding: 12,
        }
    )
})