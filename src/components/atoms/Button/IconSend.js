import React from 'react'
import {View, StyleSheet} from 'react-native'
import { IconSendDark, IconSendLight } from '../../../assets'
import { colors } from '../../../utils'

const IconSend = ({disable}) => {
    return(
        <View style={styles.container(disable)}>
            {disable && <IconSendLight />}
            {!disable && <IconSendDark />}
        </View>
    )
}

export default IconSend

const styles = StyleSheet.create({
  container: (disable) => ({
    backgroundColor: disable ? colors.tertiery : colors.textInput,
    borderRadius: 10,
    width: 45,
    height: 45,
    paddingTop: 3,
    paddingBottom: 8,
    paddingRight: 3,
    paddingLeft: 8
  }),
})
