import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { colors } from '../../../utils';
import IconOnly from './IconOnly';
import IconSend from './IconSend';

const Button = ({ type, Title, onPress, icon, disable }) => {
  if (type === 'icon-send') {
    return <IconSend disable={disable} />
  }
  if (type === 'icon-only') {
    return <IconOnly icon={icon} onPress={onPress} />
  }
  if (disable) {
    return (
      <View style={styles.disableBg}>
        <Text style={styles.disableText}>{Title}</Text>
      </View>
    )
  }
  return (
    <TouchableOpacity style={styles.container(type)} onPress={onPress}>
      <Text style={styles.teks(type)}>{Title}</Text>
    </TouchableOpacity>
  )
}

export default Button;

const styles = StyleSheet.create({
  container: type => ({
    backgroundColor: type === 'secondary' ? colors.button.secondary.background : colors.button.primary.background,
    paddingVertical: 10,
    borderRadius: 10,
  }),
  disableBg: {
    paddingVertical: 10,
    borderRadius: 10,
    backgroundColor: colors.button.disable.background
  },
  teks: type => ({
    fontSize: 16,
    fontWeight: '600',
    textAlign: 'center',
    color: type === 'secondary' ? colors.button.secondary.text : colors.button.primary.text,
  }),
  disableText: {
    fontSize: 16,
    fontWeight: '600',
    textAlign: 'center',
    color: colors.button.disable.text
  },
})
