import React from 'react'
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import { colors, Fonts } from '../../../utils';

const Link = ({label, size, align, onPress}) => {
    return(
        <TouchableOpacity onPress={onPress}>
            <Text style={styles.title(size, align)}>{label}</Text>
        </TouchableOpacity>
    )
}

export default Link;

const styles = StyleSheet.create({
    title: (size, align) => ({
        fontSize:size,
        color:colors.text.secondary,
        fontFamily:Fonts.primary[400],
        textDecorationLine:'underline',
        textAlign:align,
    })
})